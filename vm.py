import dis
import sys
import operator
import builtins


class Block:
    def __init__(self, type, prev, stack_depth):
        self.type = type
        self.prev = prev
        self.stack_depth = stack_depth


class VirtualMachine:

    def __init__(self):
        self.stack = []
        self.block_stack = []
        self.environment = {}
        self.cur_offset = 0
        self.code = None
        self.return_res = None

        self.inplace_binary = {
            'INPLACE_POWER': operator.ipow,
            'INPLACE_MULTIPLY': operator.imul,
            'INPLACE_FLOOR_DIVIDE': operator.ifloordiv,
            'INPLACE_TRUE_DIVIDE': operator.itruediv,
            'INPLACE_MODULO': operator.imod,
            'INPLACE_ADD': operator.iadd,
            'INPLACE_SUBTRACT': operator.isub,
            'INPLACE_LSHIFT': operator.ilshift,
            'INPLACE_RSHIFT': operator.irshift,
            'INPLACE_AND': operator.iand,
            'INPLACE_XOR': operator.ixor,
            'INPLACE_OR': operator.ior,
            'DELETE_SUBSCR': operator.delitem
        }

        self.unary_arithmetic = {
            'UNARY_POSITIVE': operator.pos,
            'UNARY_NEGATIVE': operator.neg,
            'UNARY_NOT': operator.not_,
            'UNARY_INVERT': operator.invert,
            'GET_ITER': self.get_iter
        }

        self.binary_arithmetic = {
            # binary arithmetic
            'BINARY_POWER': pow,
            'BINARY_MULTIPLY': operator.mul,
            'BINARY_FLOOR_DIVIDE': operator.floordiv,
            'BINARY_TRUE_DIVIDE': operator.truediv,
            'BINARY_MODULO': operator.mod,
            'BINARY_ADD': operator.add,
            'BINARY_SUBTRACT': operator.sub,
            'BINARY_SUBSCR': operator.getitem,
            'BINARY_LSHIFT': operator.lshift,
            'BINARY_RSHIFT': operator.rshift,
            'BINARY_AND': operator.and_,
            'BINARY_XOR': operator.xor,
            'BINARY_OR': operator.or_
        }
        self.cmp_op = [
            operator.lt,
            operator.le,
            operator.eq,
            operator.ne,
            operator.gt,
            operator.ge,
            self.in_,
            self.not_in,
            operator.is_,
            operator.is_not
        ]

    def parse_code(self):
        byte_code = self.code.co_code[self.cur_offset]
        self.cur_offset += 1
        byte_name = dis.opname[byte_code]
        if byte_code >= dis.HAVE_ARGUMENT:
            arg = self.code.co_code[self.cur_offset: self.cur_offset + 2]
            self.cur_offset += 2
            argval = arg[0] + (arg[1] * 256)
            # a trick to make all jumps absolute
            if byte_code in dis.hasjrel:
                arg = self.cur_offset + argval
            else:
                arg = argval
        else:
            arg = None
        return byte_name, arg

    def run_code(self, code):
        if type(code) == str:
            self.code = compile(code, '<test>', 'exec')
        else:
            self.code = code

        while True:
            instruction = self.parse_code()
            res = self.run_instruction(instruction)
            # print(res)
            if res is not None:
                break
        return self.return_res

    def run(self, code):
        self.run_code(code)

    def run_instruction(self, instruction):
        instruction_name = instruction[0]
        argval = instruction[1]

        if instruction_name in self.binary_arithmetic:
            last = self.get_lastn(2)
            operation = self.binary_arithmetic[instruction_name]
            res = self.stack.append(operation(last[0], last[1]))
        elif instruction_name in self.unary_arithmetic:
            tos = self.stack.pop()
            operation = self.unary_arithmetic[instruction_name]
            res = self.stack.append(operation(tos))
        elif instruction_name in self.inplace_binary:
            last = self.get_lastn(2)
            operation = self.inplace_binary[instruction_name]
            res = self.stack.append(operation(last[0], last[1]))
        else:
            function = getattr(self, str.lower(instruction_name))
            res = function(argval)

        return res

    def load_const(self, const_index):
        self.stack.append(self.code.co_consts[const_index])

    def store_name(self, name_index):
        val = self.stack.pop()
        self.environment[self.code.co_names[name_index]] = val

    def delete_name(self, name):
        del self.environment[name]

    def return_value(self, value):
        self.return_res = value
        return 'return'

    def load_name(self, name_index):
        name = self.code.co_names[name_index]
        if name in self.environment:
            obj = self.environment[name]
        else:
            obj = getattr(builtins, name)
        self.stack.append(obj)

    def delete_fast(self, name):
        del self.environment[name]

    def load_fast(self, var_num):
        self.stack.append(self.code.co_varnames[var_num])

    def store_fast(self, var_num):
        self.code.co_varnames[var_num] = self.stack.pop()

    def compare_op(self, op_name):
        last = self.get_lastn(2)
        self.stack.append(self.cmp_op[op_name](last[0], last[1]))

    def call_function(self, argval):
        positional = argval % 256
        keyword = int(argval / 256)
        kwargs = {}

        for i in range(keyword):
            val = self.stack.pop()
            key = self.stack.pop()
            kwargs[key] = val

        args = self.get_lastn(positional)

        self.stack.append(self.stack.pop()(*args, **kwargs))

    # stack operations
    def rot_two(self, argval):
        first = self.stack.pop()
        second = self.stack.pop()
        self.stack.append(first)
        self.stack.append(second)

    def rot_three(self, argval):
        first = self.stack.pop()
        second = self.stack.pop()
        third = self.stack.pop()

        self.stack.append(first)
        self.stack.append(third)
        self.stack.append(second)

    def pop_top(self, argval):
        self.stack.pop()

    # tos - top of stack, tos1 - second top stack
    def get_lastn(self, num):
        result = []
        for i in range(num):
            result.append(self.stack.pop())
        result.reverse()
        return result

    # jump operations
    def jump(self, argval):
        self.cur_offset = argval

    def jump_forward(self, argval):
        self.jump(argval)

    def pop_jump_if_true(self, argval):
        tos = self.stack.pop()
        if tos:
            self.jump(argval)

    def pop_jump_if_false(self, argval):
        tos = self.stack.pop()
        if not tos:
            self.jump(argval)

    def jump_if_true_or_pop(self, argval):
        if self.stack[-1]:
            self.jump(argval)
        else:
            self.stack.pop()

    def jump_if_false_or_pop(self, argval):
        if not self.stack[-1]:
            self.jump(argval)
        else:
            self.stack.pop()

    def jump_absolute(self, argval):
        self.jump(argval)

    # some cmp_op functions
    def in_(self, a, b):
        return a in b

    def not_in(self, a, b):
        return a not in b

    # iterator operations
    def for_iter(self, argval):
        iter = self.stack[-1]
        try:
            next_iter = next(iter)
            self.stack.append(next_iter)
        except StopIteration:
            self.stack.pop()
            self.jump(argval)

    def get_iter(self, tos):
        return iter(tos)

    # list, tuple etc
    def build_list(self, num):
        self.stack.append(self.get_lastn(num))

    def build_tuple(self, num):
        self.stack.append(tuple(self.get_lastn(num)))

    def build_set(self, num):
        self.stack.append(set(self.get_lastn(num)))

    def build_map(self, num):
        self.stack.append({})

    def list_append(self, i):
        list.append(self.stack[-i], self.stack.pop())

    def set_add(self, i):
        set.add(self.stack[-i], self.stack.pop())

    def map_add(self, i):
        val, key = self.get_lastn(2)
        dict.__setitem__(self.stack[-i], key, val)

    def store_map(self, i):
        map_, val, key = self.get_lastn(3)
        map_[key] = val
        self.stack.append(map_)

    def store_subscr(self, argval):
        tos2, tos1, tos = self.get_lastn(3)
        tos1[tos] = tos2
        self.stack.append(tos1)

    def load_attr(self, name_index):
        tos = self.stack.pop()
        self.stack.append(getattr(tos, self.code.co_names[name_index]))

    def store_attr(self, name_index):
        tos1, tos = self.get_lastn(2)
        setattr(tos, name_index, tos1)

    def delete_attr(self, name_index):
        tos = self.get_lastn(1)
        delattr(tos, name_index)

    def unpack_sequence(self, count):
        top = self.get_lastn(1)
        top.reverse()
        for x in top:
            self.stack.append(x)

    def build_slice(self, argc):
        if argc == 2:
            tos1, tos = self.get_lastn(2)
            self.stack.append(slice(tos1, tos))
        else:
            tos2, tos1, tos = self.get_lastn(3)
            self.stack.append(slice(tos2, tos1, tos))

    # loops
    def setup_loop(self, argval):
        self.block_stack.append(Block('loop', argval, len(self.stack)))

    def continue_loop(self, argval):
        self.return_res = argval
        return 'continue'

    def break_loop(self, argval):
        return 'break'

    def pop_block(self, argval):
        self.block_stack.pop()


if __name__ == "__main__":
    compiled = compile(sys.stdin.read(), "<stdin>", 'exec')
    VirtualMachine().run(compiled)
